# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

<table>
  <tr>
    <th></th>
    <th colspan="2">Cycles/Ops</th>
    <th colspan="2">Time/Op (us)</th>
    <th colspan="2">Ops/second</th>
    <th colspan="2">Total Ops/second</th>
  </tr>
  <tr>
    <td>No. of threads</td>
    <td>Lock</td>
    <td>CAS</td>
    <td>Lock</td>
    <td>CAS</td>
    <td>Lock</td>
    <td>CAS</td>
    <td>Lock</td>
    <td>CAS</td>
  </tr>
  <tr>
    <td>2</td>
    <td>763</td>
    <td>213</td>
    <td>0.246</td>
    <td>0.068</td>
    <td>4,060,996</td>
    <td>14,517,482</td>
    <td>16,243,985</td>
    <td>58,069,929</td>
  </tr>
  <tr>
    <td>4</td>
    <td>916</td>
    <td>139</td>
    <td>0.295</td>
    <td>0.045</td>
    <td>3,383,042</td>
    <td>22,298,145</td>
    <td>27,064,339</td>
    <td>178,385,162</td>
  </tr>
  <tr>
    <td>6</td>
    <td>1927</td>
    <td>202</td>
    <td>0.622</td>
    <td>0.065</td>
    <td>1,608,072</td>
    <td>15,325,914</td>
    <td>19,296,866</td>
    <td>183,910,968</td>
  </tr>
  <tr>
    <td>8</td>
    <td>4694</td>
    <td>262</td>
    <td>1.514</td>
    <td>0.085</td>
    <td>660,282</td>
    <td>11,787,579</td>
    <td>10,564,505</td>
    <td>188,601,264</td>
  </tr>
  <tr>
    <td>10</td>
    <td>7176</td>
    <td>408</td>
    <td>2.315</td>
    <td>0.132<br></td>
    <td>431,922</td>
    <td>7,595,335</td>
    <td>8,638,448</td>
    <td>151,906,708</td>
  </tr>
</table>